#if(!${entityPackage})
package ${bussPackage}.dao;
#else
package ${bussPackage}.dao.${entityPackage};
#end

import com.afcatstar.commons.BaseDao;
#if(!${entityPackage})
import ${bussPackage}.domain.${className}Domain;
#else
import ${bussPackage}.domain.${entityPackage}.${className};
#end

public interface ${className}Mapper extends BaseDao<${className}Domain> {
}
