#if(!${entityPackage})
package ${bussPackage}.service.impl;
#else
package ${bussPackage}.service.impl.${entityPackage};
#end

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import com.afcatstar.commons.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
#if(!${entityPackage})
import ${bussPackage}.dao.${className}Mapper;
#else
import ${bussPackage}.dao.${entityPackage}.${className}Mapper;
#end
#if(!${entityPackage})
import ${bussPackage}.service.${className}Service;
#else
import ${bussPackage}.service.${entityPackage}.${className}Service;
#end
#if(!${entityPackage})
import ${bussPackage}.domain.${className}Domain;
#else
import ${bussPackage}.domain.${entityPackage}.${className}Domain;
#end
@Service("$!{lowerName}Service")
public class ${className}ServiceImpl extends BaseService<${className}Mapper,${className}Domain> implements ${className}Service {
	private final static Logger LOG = LoggerFactory.getLogger(${className}ServiceImpl.class);

    
    public void add(${className}Domain t){
    	super.add(t);
    }

    public void update(${className}Domain t){
    	super.update(t);
    }

    public void updateBySelective(${className}Domain t){
    	super.updateBySelective(t);
    }

    public void delete(Object id){
    	super.delete(id);
    }

    public int queryByCount(${className}Domain t){
    	return super.queryByCount(t);
    }

    public List<${className}Domain> queryByList(${className}Domain t){
    	return super.queryByList(t);
    }

    public ${className}Domain queryById(Object id){
    	return super.queryById(id);
    }
    
}
