#if(!${entityPackage})
package ${bussPackage}.service;
#else
package ${bussPackage}.service.${entityPackage};
#end

#if(!${entityPackage})
import ${bussPackage}.domain.${className}Domain;
#else
import ${bussPackage}.domain.${entityPackage}.${className}Domain;
#end

import java.util.List;

public interface ${className}Service {

    public void add(${className}Domain t);

    public void update(${className}Domain t);

    public void updateBySelective(${className}Domain t);

    public void delete(Object id);

    public int queryByCount(${className}Domain t);

    public List<${className}Domain> queryByList(${className}Domain t);

    public ${className}Domain queryById(Object id);
}
