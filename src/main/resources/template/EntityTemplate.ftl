#if(!${entityPackage})
package ${bussPackage}.domain;
#else
package ${bussPackage}.domain.${entityPackage};
#end

import com.afcatstar.commons.BaseDomain;
import java.math.BigDecimal;
import java.util.Date;

public class ${className}Domain extends BaseDomain<${className}Domain> {

	private static final long serialVersionUID = 1L;	
	${feilds}
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("${className} [");   
#foreach($item in $!{columnDatas})
#if($velocityCount == 1)
        builder.append("$!item.fieldName=");
#else
        builder.append(", $!item.fieldName=");
#end
        builder.append($!item.fieldName);
#end
        builder.append("]");
        return builder.toString();
    }
    
   	public Long getId(){
   		return null;
   	}

	public void setId(Long id){
	
	}
}

