package com.afcatstar.text.label.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import com.afcatstar.commons.BaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.afcatstar.text.label.dao.TextLabelConfigMapper;
import com.afcatstar.text.label.service.TextLabelConfigService;
import com.afcatstar.text.label.domain.TextLabelConfigDomain;
@Service("textLabelConfigService")
public class TextLabelConfigServiceImpl extends BaseService<TextLabelConfigMapper,TextLabelConfigDomain> implements TextLabelConfigService {
	private final static Logger LOG = LoggerFactory.getLogger(TextLabelConfigServiceImpl.class);

    
    public void add(TextLabelConfigDomain t){
    	super.add(t);
    }

    public void update(TextLabelConfigDomain t){
    	super.update(t);
    }

    public void updateBySelective(TextLabelConfigDomain t){
    	super.updateBySelective(t);
    }

    public void delete(Object id){
    	super.delete(id);
    }

    public int queryByCount(TextLabelConfigDomain t){
    	return super.queryByCount(t);
    }

    public List<TextLabelConfigDomain> queryByList(TextLabelConfigDomain t){
    	return super.queryByList(t);
    }

    public TextLabelConfigDomain queryById(Object id){
    	return super.queryById(id);
    }
    
}
