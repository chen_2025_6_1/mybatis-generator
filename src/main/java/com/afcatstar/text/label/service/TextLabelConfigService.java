package com.afcatstar.text.label.service;

import com.afcatstar.text.label.domain.TextLabelConfigDomain;

import java.util.List;

public interface TextLabelConfigService {

    public void add(TextLabelConfigDomain t);

    public void update(TextLabelConfigDomain t);

    public void updateBySelective(TextLabelConfigDomain t);

    public void delete(Object id);

    public int queryByCount(TextLabelConfigDomain t);

    public List<TextLabelConfigDomain> queryByList(TextLabelConfigDomain t);

    public TextLabelConfigDomain queryById(Object id);
}
