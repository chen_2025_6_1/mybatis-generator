package com.afcatstar.text.label.dao;

import com.afcatstar.commons.BaseDao;
import com.afcatstar.text.label.domain.TextLabelConfigDomain;

public interface TextLabelConfigMapper extends BaseDao<TextLabelConfigDomain> {
}
