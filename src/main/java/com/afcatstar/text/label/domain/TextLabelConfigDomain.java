package com.afcatstar.text.label.domain;

import com.afcatstar.commons.BaseDomain;
import java.math.BigDecimal;
import java.util.Date;

public class TextLabelConfigDomain extends BaseDomain<TextLabelConfigDomain> {

	private static final long serialVersionUID = 1L;	
		private Long id;//   
	private String textCategory;//   
	private Long labelId;//   
	private Long libraryId;//   
	public Long getId() {	    return this.id;	}	public void setId(Long id) {	    this.id = id;	}	public String getTextCategory() {	    return this.textCategory;	}	public void setTextCategory(String textCategory) {	    this.textCategory = textCategory;	}	public Long getLabelId() {	    return this.labelId;	}	public void setLabelId(Long labelId) {	    this.labelId = labelId;	}	public Long getLibraryId() {	    return this.libraryId;	}	public void setLibraryId(Long libraryId) {	    this.libraryId = libraryId;	}
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TextLabelConfig [");   
        builder.append("id=");
        builder.append(id);
        builder.append(", textCategory=");
        builder.append(textCategory);
        builder.append(", labelId=");
        builder.append(labelId);
        builder.append(", libraryId=");
        builder.append(libraryId);
        builder.append("]");
        return builder.toString();
    }
    
   	public Long getId(){
   		return null;
   	}

	public void setId(Long id){
	
	}
}

